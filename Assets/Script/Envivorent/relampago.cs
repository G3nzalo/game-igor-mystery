﻿using System.Collections;
using UnityEngine;

namespace Juego.Envivorent
{
    public class relampago : MonoBehaviour
    {
        private Light luz = null;

        void Awake()
        {
            luz = GetComponent<Light>();
        }

        void Update()
        {
            StartCoroutine(RelampagoDeLuz());
        }

        IEnumerator RelampagoDeLuz()
        {
            yield return new WaitForSeconds(5f);
            luz.intensity = Random.Range(0.2f, 1.5f);
            yield return new WaitForSeconds(1f);
            luz.intensity = 0.5f;
        }

    }
}
