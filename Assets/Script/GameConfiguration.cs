﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameConfiguration", menuName = "GameConfiguration", order = 0)]

public class GameConfiguration : ScriptableObject
{
    [Header("Seccion Enemigos")]
    public int cantidadEnemigos;
    public float vidaEnemigos;
    public float ataqueEnemigos;

    [Header("Seccion Player")]
    public float ataquePlayer;
    public float startVidaPlayer;

    [Space]
    [Header("Movimientos player")]
    public float speedPlayer;
    public float rotacionPower;
    public float rotacionLerp;

    [Space]
    [Header("Seccion Controles")]
    public string horizontal;
    public string vertical;
    public string corre;

}
