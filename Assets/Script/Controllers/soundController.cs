﻿using UnityEngine;

namespace Juego.Controllers
{
    public class soundController : MonoBehaviour
    {
        public static soundController instance = null;

        #region Referencias En Unity
        [Header("Zona Track ")]
        [SerializeField] private AudioSource tracks_SourceNivel = null;
        [SerializeField] private AudioClip trackNivel1 = null;


        [Header("Main Menu ")]
        [SerializeField] private AudioSource trackMenu = null;
        [SerializeField] private AudioClip trackMenuMain = null;


        [Header("Zona sFx")]
        [SerializeField] private AudioSource[] sfxSource = null;

        [SerializeField] private AudioClip[] sfxPasos = null;


        [SerializeField] private AudioClip disparo = null;

        [SerializeField] private AudioClip btnMenu = null;
        #endregion

        void Awake()
        {
            Initialice();
        }

        #region Public Mehotds
        public void PlaySfxCaminaYCorre(int indexClip, float volumen, float pitch)
        {

            sfxSource[0].clip = sfxPasos[indexClip];
            sfxSource[0].volume = volumen;
            sfxSource[0].pitch = pitch;

            if (sfxSource[0].isPlaying == false)
            {
                sfxSource[0].Play();
            }
        }

        public void PlayOneShotDisparo(float volumen, float pitch)
        {
            sfxSource[1].volume = volumen;
            sfxSource[1].pitch = pitch;
            sfxSource[1].PlayOneShot(disparo);

        }

        public void PlayOneShotBtnMenu(float volumen, float pitch)
        {
            sfxSource[2].volume = volumen;
            sfxSource[2].pitch = pitch;
            sfxSource[2].PlayOneShot(btnMenu);

        }

        public void SetPitch(int index, float value)
        {
            sfxSource[index].pitch = value;
        }

        public void SetPausa(int index)
        {
            sfxSource[index].Pause();
        }

        public void SubirVolume()
        {
            AudioSource[] audios = FindObjectsOfType<AudioSource>();

            foreach (AudioSource i in audios)
            {
                if (i.volume < 100f)
                {
                    i.volume += 0.05f;
                }
            }
        }

        public void BajarVolume()
        {
            AudioSource[] audios = FindObjectsOfType<AudioSource>();

            foreach (AudioSource i in audios)
            {

                i.volume -= 0.05f;

                if (i.volume < 0.10f)
                {
                    i.volume = 0.10f;
                }

            }
        }

        public float GetVolume()
        {
            return Mathf.Round(tracks_SourceNivel.volume * 100.0f);
        }

        public void SetTrackNivel1(float volume)
        {
            tracks_SourceNivel.clip = trackNivel1;
            tracks_SourceNivel.volume = volume;
            tracks_SourceNivel.Play();
        }

        public void SetTrackMainMenu(float volume)
        {
            trackMenu.clip = trackMenuMain;
            trackMenu.volume = volume;
            trackMenu.Play();

        }

        public void SetPausaTrackMenu()
        {
            trackMenu.Pause();
        }

        public void SetPausaTrackNivel()
        {
            tracks_SourceNivel.Pause();
        }

        public void SetPlayTrackNivel()
        {
            tracks_SourceNivel.Play();
        }

        public void SetPlayTrackMenu()
        {
            trackMenu.Play();
        }
        #endregion

        private void Initialice()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

    }
}
