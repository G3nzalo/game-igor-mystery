﻿using System.Collections;
using UnityEngine;

namespace Juego.Controllers
{

    public class controller_cam : MonoBehaviour
    {
        [SerializeField] private GameObject mainCamera = null;
        [SerializeField] private GameObject camaraDisparo = null;
        [SerializeField] private GameObject mira = null;

        void Update()
        {
            if (Input.GetMouseButton(1) && !camaraDisparo.activeInHierarchy)
            {
                mainCamera.SetActive(false);
                camaraDisparo.SetActive(true);
                //Permitir que la camara se mezcle y despues activo la mira por eso la corrutina
                StartCoroutine(MiraPrendida());
            }

            if (Input.GetMouseButtonUp(1) && !mainCamera.activeInHierarchy)
            {
                mainCamera.SetActive(true);
                camaraDisparo.SetActive(false);
                mira.SetActive(false);
            }
        }
        IEnumerator MiraPrendida()
        {
            yield return new WaitForSeconds(0.25f);
            mira.SetActive(enabled);
        }
    }
}
