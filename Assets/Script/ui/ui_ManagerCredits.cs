﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Controllers;

namespace Juego.Ui
{
    public class ui_ManagerCredits : MonoBehaviour
    {

        public void QuitGame()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            Application.Quit();
        }

        public void MainMenu()
        {
            soundController.instance.SetPausaTrackNivel();
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            soundController.instance.SetPlayTrackMenu();
            Time.timeScale = 1;
            SceneManager.LoadScene(1);
        }


    }
}
