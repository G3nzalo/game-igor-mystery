﻿using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using Juego.Entidades;
using Juego.Controllers;

namespace Juego.Ui
{
    public class ui_Manager : MonoBehaviour
    {
        #region Referencia en Scena 
        [SerializeField] private mov player = null;

        public static ui_Manager instance = null;

        public TextMeshProUGUI numEnemies = null;
        public TextMeshProUGUI vida1 = null;
        public GameObject optionsPanel = null;
        public GameObject panelInfo = null;

        public GameObject panelAjustes = null;
        public GameObject panelControls = null;
        public GameObject panelSound = null;

        public TextMeshProUGUI currentLevelAudio = null;
        #endregion

        #region Private Vars
        private bool enPausa = false;
        private int numEnemiesInit = 0;
        #endregion

        #region Unity Calls
        void Awake()
        {
            Initialize();
        }

        void Start()
        {
            numEnemiesInit = player.GetEnemiesValues();
            numEnemies.text = numEnemiesInit.ToString();
            vida1.text = player.GetVida().ToString();
        }

        private void Update()
        {
            OptionsPanelDesdeTeclado();
            currentLevelAudio.text = soundController.instance.GetVolume().ToString();
        }
        #endregion

        #region Public Methods
        public void SetCurNumEnemies(int enemies)
        {
            numEnemiesInit -= enemies;

            if (numEnemiesInit < 0)
            {
                numEnemiesInit = 0;
            }

            numEnemies.text = numEnemiesInit.ToString();
        }

        public void SetVidaActual(int vidaActual)
        {
            vida1.text = vidaActual.ToString();
        }

        public void OptionsPanelDesdeTeclado()
        {
            if (Input.GetKey(KeyCode.Escape) && !enPausa)
            {
                soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
                enPausa = true;
                Time.timeScale = 0;
                panelInfo.SetActive(false);
                optionsPanel.SetActive(true);

                AudioSource[] audios = FindObjectsOfType<AudioSource>();

                foreach (AudioSource i in audios)
                {
                    i.Pause();
                }
            }
        }

        public void ContinuarJuego()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f); enPausa = false;
            Time.timeScale = 1;
            panelInfo.SetActive(true);
            optionsPanel.SetActive(false);

            AudioSource[] audios = FindObjectsOfType<AudioSource>();

            foreach (AudioSource i in audios)
            {
                i.Play();
            }
            soundController.instance.SetPausaTrackMenu();
        }

        public void Ajustes()
        {
            panelAjustes.SetActive(true);
            optionsPanel.SetActive(false);
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
        }

        public void MainMenu()
        {
            soundController.instance.SetPausaTrackNivel();
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f); enPausa = false;
            Time.timeScale = 1;
            optionsPanel.SetActive(false);
            SceneManager.LoadScene(1);
            soundController.instance.SetPlayTrackMenu();
        }

        public void QuitGame()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            Application.Quit();
        }

        public void ControlsPanel()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            panelControls.SetActive(true);
            panelAjustes.SetActive(false);
        }

        public void SoundPanel()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            panelSound.SetActive(true);
            panelAjustes.SetActive(false);
            AudioSource[] audios = FindObjectsOfType<AudioSource>();

            foreach (AudioSource i in audios)
            {
                i.Play();
            }
        }

        public void SubirMusic()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            soundController.instance.SubirVolume();
        }

        public void BajarMusic()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            soundController.instance.BajarVolume();
        }

        public void BackAjustes()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            soundController.instance.SetPlayTrackMenu();
            AudioSource[] audios = FindObjectsOfType<AudioSource>();

            foreach (AudioSource i in audios)
            {
                i.Pause();
            }
            panelControls.SetActive(false);
            panelSound.SetActive(false);
            panelAjustes.SetActive(true);
        }

        public void BackMenuGame()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            panelAjustes.SetActive(false);
            optionsPanel.SetActive(true);
        }

        public bool GetEnPausa()
        {
            return enPausa;
        }
        #endregion

        private void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }

            else
            {
                Destroy(gameObject);
            }
        }
    }
}
