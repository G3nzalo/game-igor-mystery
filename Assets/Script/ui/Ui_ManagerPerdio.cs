﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Controllers;

namespace Juego.Ui
{
    public class Ui_ManagerPerdio : MonoBehaviour
    {

        public void PlayGame()
        {
            //scene Game
            soundController.instance.SetPausaTrackNivel();
            soundController.instance.SetPausaTrackMenu();
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            SceneManager.LoadScene(2);
            soundController.instance.SetPlayTrackNivel();
        }

        public void QuitGame()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            Application.Quit();
        }

        public void MainMenu()
        {
            soundController.instance.SetPausaTrackNivel();
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            Time.timeScale = 1;
            SceneManager.LoadScene(1);
            soundController.instance.SetPlayTrackMenu();
        }


    }
}
