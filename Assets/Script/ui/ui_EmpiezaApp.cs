﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Controllers;

namespace Juego.Ui
{
    public class ui_EmpiezaApp : MonoBehaviour
    {
        private void Start()
        {
            soundController.instance.SetTrackMainMenu(0.3f);
        }

        public void MainMenu()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            SceneManager.LoadScene(1);
        }

    }
}