﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Controllers;

namespace Juego.Ui
{
    public class ui_ManagerMainMenu : MonoBehaviour
    {
        #region Public Referencia Objetos
        public GameObject panelMainMenu = null;
        public GameObject panelControls = null;
        public GameObject panelCredits = null;
        #endregion

        #region Public Methods
        public void PlayGame()
        {
            //scene Game
            soundController.instance.SetPausaTrackMenu();
            SceneManager.LoadScene(2);
            soundController.instance.SetTrackNivel1(0.3f);
        }

        public void ControlsPanel()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            panelControls.SetActive(true);
            panelMainMenu.SetActive(false);
        }
        public void CreditsPanel()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            panelCredits.SetActive(true);
            panelMainMenu.SetActive(false);
        }

        public void BackMenuGame()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            soundController.instance.SetPlayTrackMenu();
            panelControls.SetActive(false);
            panelCredits.SetActive(false);
            panelMainMenu.SetActive(true);
        }

        public void QuitGame()
        {
            soundController.instance.PlayOneShotBtnMenu(0.1f, 1f);
            Application.Quit();
        }
        #endregion
    }
}
