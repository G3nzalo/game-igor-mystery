﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Juego.Entidades;

namespace Juego.Animations
{
    public class animation_Controller : MonoBehaviour
    {
        public static animation_Controller instance = null;

        [SerializeField] private mov player = null;


        private Animator anim = null;

        private void Awake()
        {
            Initialice();
            anim = GetComponent<Animator>();
        }

        void Update()
        {
            SetAnimMovimientos();
            SetAnimDisparo();
            SetAnimGano();
            SetAnimMuere();
        }

        #region Private Methods
        private void SetAnimCaminaCorreAdelante()
        {
            //Camina Corre Adelante
            if (player.moveVertical == 1f && player.moveHorizontal==0)
            {
                anim.SetBool("camina", true);

                if (player.corre == 1f)
                {
                    anim.SetBool("corriendo", true);
                }

                else
                {
                    anim.SetBool("corriendo", false);
                }
            }

            if (player.moveVertical <= 0)
            {
                anim.SetBool("camina", false);
                anim.SetBool("corriendo", false);
            }
        }

        private void SetAnimaCaminaCorreAtras()
        {
            //Camina Corre Atras
            if (player.moveVertical == -1f && player.moveHorizontal == 0)
            {
                anim.SetBool("caminaAtras", true);

                if (player.corre == 1f)
                {
                    anim.SetBool("correrAtras", true);
                }

                else
                {
                    anim.SetBool("correrAtras", false);
                }
            }

            if (player.moveVertical >= 0)
            {
                anim.SetBool("caminaAtras", false);
                anim.SetBool("correrAtras", false);
            }
        }

        private void SetAnimCaminaCorreIzquierda()
        {
            //Camina Corre Izquierda
            if (player.moveHorizontal == -1f && player.moveVertical==0)
            {
                anim.SetBool("caminaIzquierda", true);

                if (player.corre == 1f)
                {
                    anim.SetBool("correrIzquierda", true);
                }
              
                else
                {
                    anim.SetBool("correrIzquierda", false);
                }

            }
          
            if (player.moveHorizontal >= 0)
            {
                anim.SetBool("caminaIzquierda", false);
                anim.SetBool("correrIzquierda", false);
            }
        }

        private void SetAnimCaminaCorreDerecha()
        {
            //Camina corre Derecha
            if (player.moveHorizontal == 1f && player.moveVertical==0)
            {
                anim.SetBool("caminaDerecha", true);

                if (player.corre == 1f)
                {
                    anim.SetBool("correrDerecha", true);
                }

                else
                {
                    anim.SetBool("correrDerecha", false);
                }
            }

            if (player.moveHorizontal <= 0)
            {
                anim.SetBool("caminaDerecha", false);
                anim.SetBool("correrDerecha", false);
            }
        }

        private void SetAnimParado()
        {
            //Esta parado
            if (player.moveVertical == 0f && player.moveHorizontal == 0f)
            {
                anim.SetBool("camina", false);
                anim.SetBool("caminaAtras", false);
                anim.SetBool("caminaDerecha", false);
                anim.SetBool("caminaIzquierda", false);

                anim.SetBool("corriendo", false);
                anim.SetBool("correrAtras", false);
                anim.SetBool("correrIzquierda", false);
                anim.SetBool("correrDerecha", false);

            }

            if (player.corre == 0f)
            {
                anim.SetBool("corriendo", false);
            }
        }

        private void SetAnimMovimientos()
        {
            SetAnimParado();
            SetAnimCaminaCorreAdelante();
            SetAnimaCaminaCorreAtras();
            SetAnimCaminaCorreIzquierda();
            SetAnimCaminaCorreDerecha();
        }

        private void SetAnimGano()
        {
            if (player.GetEnemiesValues() == 0)
            {
                anim.SetBool("gano", true);
                StartCoroutine(SegundosDeFestejo());

            }
        }

        private void SetAnimMuere()
        {
            if (player.GetVida() <= 0)
            {
                anim.SetBool("muere", true);
                StartCoroutine(SegundosQueMuere());
            }
        }

        private void SetAnimDisparo()
        {
            if (Input.GetMouseButton(1))
            {
                anim.SetBool("disparo", true);
            }

            else
            {
                anim.SetBool("disparo", false);
            }

        }

        private void Initialice()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        #endregion



        IEnumerator SegundosDeFestejo()
        {
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene(3);
        }

        IEnumerator SegundosQueMuere()
        {
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene(4);
        }


    }
}
