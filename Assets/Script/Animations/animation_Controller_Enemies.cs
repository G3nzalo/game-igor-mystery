﻿using UnityEngine;
using Juego.Entidades;

namespace Juego.Animations
{
    public class animation_Controller_Enemies : MonoBehaviour
    {
        [SerializeField] private EnemigoController enemigo = null;
        [SerializeField] private mov player = null;

        private Animator anim = null;

        #region Unity Calls
        private void Awake()
        {
            anim = GetComponent<Animator>();
        }

        private void Update()
        {
            if (enemigo.GetVida() <= 0)
            {
                anim.SetBool("muere", true);
            }

            SetDificultad();
        }

        public void OnTriggerStay(Collider collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                SetAnimAtaca(true);
                player.ReciveAtack(enemigo.GetAtaque());
            }
        }

        public void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                SetAnimAtaca(false);
            }
        }
        #endregion

        private void SetAnimAtaca(bool collisiona)
        {
            if (collisiona)
            {
                anim.SetBool("ataca", true);
            }
            else
            {
                anim.SetBool("ataca", false);
            }
        }

        private void SetDificultad()
        {
            if (enemigo.GetVida() <= 5)
            {
                anim.SetBool("corre", true);
            }
        }

    }
}
