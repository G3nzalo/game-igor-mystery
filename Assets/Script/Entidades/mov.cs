﻿using UnityEngine;
using Juego.Controllers;
using Juego.Ui;

namespace Juego.Entidades
{
    public class mov : MonoBehaviour
    {
        #region Referencia En scene
        [SerializeField] private GameConfiguration gameConfig = null;

        [Space]
        [SerializeField] private GameObject dondeApuntaLaCamara = null;
        #endregion


        #region Private Vars
        private Rigidbody rb = null;
        private Vector3 nextPosition = Vector3.zero;
        private Quaternion nextRotation = Quaternion.identity;

        private float vida = 0f;
        private int startCantidadEnemies = 0;
        #endregion

        #region Public Vars
        public float moveHorizontal;
        public float moveVertical;
        public float corre;
        public Vector2 look;
        public float apunta;
        public float fire;
        public float speedH;
        public float speedV;
        #endregion


        #region Public Methods
        public int GetEnemiesValues()
        {
            return startCantidadEnemies;
        }

        public int GetVida()
        {
            return (int)vida;
        }

        public void SetMuereEnemigo(int enemigo)
        {
            startCantidadEnemies -= enemigo;
        }

        public void ReciveAtack(float ataque)
        {
            vida -= ataque;
            if (vida < 0f)
            {
                vida = 0f;
            }

            ui_Manager.instance.SetVidaActual((int)vida);
        }
        #endregion


        #region Unity Calls
        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            vida = gameConfig.startVidaPlayer;
            startCantidadEnemies = gameConfig.cantidadEnemigos;
        }

        private void Update()
        {
            ReadAxisValues();
            RotacionPersonajeCamara();
            CheckInputSound(moveHorizontal, moveVertical);
        }

        private void FixedUpdate()
        {
            MoverPersonaje(moveHorizontal, moveVertical);
        }
        #endregion


        #region Movimientos Del Personajes 
        private void ReadAxisValues()
        {
            moveHorizontal = Input.GetAxis(gameConfig.horizontal);
            moveVertical = Input.GetAxis(gameConfig.vertical);
            corre = Input.GetAxis(gameConfig.corre);
            look = Input.mousePosition * Time.deltaTime;
        }

        private void MoverPersonaje(float axisX, float axisY)
        {
            if (axisX != 0f && axisY == 0f)
            {
                MoverPersonajeHorizontal(axisX);

                if (corre != 0f)
                {
                    CorrerHorizontal(axisX);
                }

            }
            if (axisY != 0f && axisX == 0)
            {
                MoverPersonajeVertical(axisY);

                if (corre != 0f)
                {
                    CorrerVertical(axisY);

                }
            }

        }

        private void MoverPersonajeVertical(float axis)
        {
            rb.velocity = transform.forward * axis * gameConfig.speedPlayer * Time.deltaTime;

        }

        private void MoverPersonajeHorizontal(float axis)
        {
            rb.velocity = transform.right * axis * gameConfig.speedPlayer * Time.deltaTime;
        }

        private void CorrerHorizontal(float axisCamina)
        {
            rb.velocity = transform.right * axisCamina * gameConfig.speedPlayer * 2 * Time.deltaTime;
        }

        private void CorrerVertical(float axisCamina)
        {
            rb.velocity = transform.forward * axisCamina * gameConfig.speedPlayer * 2 * Time.deltaTime;
        }

        private void RotacionPersonajeCamara()
        {
            if (ui_Manager.instance.GetEnPausa() == false)
            {

                #region horizontal rotation
                dondeApuntaLaCamara.transform.rotation *= Quaternion.AngleAxis(Input.GetAxis("Mouse X") * gameConfig.rotacionPower, Vector3.up);
                #endregion

                #region vertical rotation
                dondeApuntaLaCamara.transform.rotation *= Quaternion.AngleAxis(-Input.GetAxis("Mouse Y") * gameConfig.rotacionPower, Vector3.right);
                #endregion

                #region Delimitacion de angulo de Camara
                var angles = dondeApuntaLaCamara.transform.localEulerAngles;

                angles.z = 0;

                var angle = dondeApuntaLaCamara.transform.localEulerAngles.x;

                if (angle > 180 && angle < 340)
                {
                    angles.x = 340;
                }
                if (angle < 180 && angle > 40)
                {
                    angles.x = 40;
                }

                dondeApuntaLaCamara.transform.localEulerAngles = angles;
                #endregion

                //proxima rot
                nextRotation = Quaternion.Lerp(dondeApuntaLaCamara.transform.rotation, nextRotation, Time.deltaTime * gameConfig.rotacionLerp);

                if (moveHorizontal == 0 && moveVertical == 0)
                {
                    nextPosition = transform.position;

                    if (apunta == 1)
                    {
                        //rotacion del jugador depende donde miramos
                        transform.rotation = Quaternion.Euler(0, dondeApuntaLaCamara.transform.rotation.eulerAngles.y, 0);
                        //reset la rotacion donde mira la cam
                        dondeApuntaLaCamara.transform.localEulerAngles = new Vector3(angles.x, 0, 0);

                    }
                    return;
                }


                //set player rotacion dependiendo donde se mira cuando no apunta
                transform.rotation = Quaternion.Euler(0, dondeApuntaLaCamara.transform.rotation.eulerAngles.y, 0);

                //reset la rotacion y donde mira
                dondeApuntaLaCamara.transform.localEulerAngles = new Vector3(angles.x, 0, 0);
            }
        }
        #endregion

        private void CheckInputSound(float axisX, float axisY)
        {
            if (axisX == 1f || axisX == -1f || axisY == 1f || axisY == -1f)
            {
                soundController.instance.PlaySfxCaminaYCorre(Random.Range(0, 1), Random.Range(0.1f, 0.4f), Random.Range(0.7f, 1f));

                if (Input.GetButton(gameConfig.corre))
                {
                    soundController.instance.SetPitch(0, Random.Range(1f, 1.5f));
                }

            }

            if (axisX == 0f && axisX == 0f && axisY == 0f && axisY == 0f)
            {
                soundController.instance.SetPausa(0);
            }
        }


    }
}

