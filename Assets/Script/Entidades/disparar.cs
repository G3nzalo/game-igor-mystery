﻿using System.Collections;
using UnityEngine;
using Juego.Controllers;

namespace Juego.Entidades
{
    public class disparar : MonoBehaviour
    {
        #region Ref Scene
        [SerializeField] private GameObject dondeMira = null;

        [SerializeField] private Transform lugarDisparo = null;

        [SerializeField] private GameObject balaPrefab = null;

        [SerializeField] private GameObject particulasPrefab = null;
        #endregion

        void Update()
        {
            CheckDisparo();
        }

        private void CheckDisparo()
        {
            if (Input.GetMouseButton(1))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    StartCoroutine(Disparo());
                }
            }
        }

        IEnumerator Disparo()
        {
            GameObject balaDisparada = Instantiate(balaPrefab);
            Instantiate(particulasPrefab, lugarDisparo.position, Quaternion.identity);
            soundController.instance.PlayOneShotDisparo(Random.Range(0.2f, 0.35f), Random.Range(0.7f, 0.9f));
            balaDisparada.transform.forward = dondeMira.transform.forward;
            balaDisparada.transform.position = lugarDisparo.position + lugarDisparo.forward;

            yield return new WaitForSeconds(15f);
        }

    }
}
