﻿using System.Collections;
using UnityEngine;
using Juego.Ui;

namespace Juego.Entidades
{
    public class EnemigoController : MonoBehaviour
    {
        #region Referencis Scene
        [SerializeField] private mov player = null;
        [SerializeField] private GameObject particulasPrefabSangre = null;
        [SerializeField] private GameConfiguration gameConfig = null;
        #endregion

        #region Private Vars
        private Collider col = null;
        private float vida = 0f;
        private float ataque = 0f;
        #endregion

        #region Unity Calls
        void Awake()
        {
            col = GetComponent<Collider>();
            vida = gameConfig.vidaEnemigos;
            ataque = gameConfig.ataqueEnemigos;
        }

        public void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Bala"))
            {
                Vector3 point = collision.collider.ClosestPoint(collision.transform.position);
                Instantiate(particulasPrefabSangre, point, Quaternion.identity);
                SacaVida();

            }
        }
        #endregion

        #region Public Methods
        public float GetVida()
        {
            return vida;
        }

        public float GetAtaque()
        {
            return ataque;
        }
        #endregion

        private void SacaVida()
        {
            if (vida > 0f)
            {
                vida -= gameConfig.ataquePlayer;
            }

            if (vida <= 0f)
            {
                col.enabled = false;
                vida = 0f;
                StartCoroutine(EliminarCuerpo());
            }
        }

        IEnumerator EliminarCuerpo()
        {
            player.SetMuereEnemigo(1);
            ui_Manager.instance.SetCurNumEnemies(1);
            yield return new WaitForSeconds(2.5f);
            Destroy(gameObject);
        }

    }
}