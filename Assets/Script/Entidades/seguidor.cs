﻿using UnityEngine;
using UnityEngine.AI;

namespace Juego.Entidades
{
    public class seguidor : MonoBehaviour
    {

        [SerializeField] private Transform target = null;

        private NavMeshAgent agent = null;
        private EnemigoController controller = null;

        private void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
            controller = GetComponent<EnemigoController>();
        }

        private void Update()
        {
            agent.SetDestination(target.position);

            if (controller.GetVida() == 0)
            {
                agent.SetDestination(transform.position);
            }

        }

    }
}
