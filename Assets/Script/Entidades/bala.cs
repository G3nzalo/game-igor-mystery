﻿using System.Collections;
using UnityEngine;

namespace Juego.Entidades
{
    public class bala : MonoBehaviour
    {
        [SerializeField] private float fuerza = 6000f;

        private Rigidbody rb = null;

        Cinemachine.CinemachineImpulseSource source = null;
        Camera main = null;

        #region Unity Calls
        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            source = GetComponent<Cinemachine.CinemachineImpulseSource>();
            main = FindObjectOfType<Camera>();
        }

        void Start()
        {
            rb.isKinematic = false;
            rb.centerOfMass = transform.position;
            rb.AddForce(transform.forward * fuerza * Time.deltaTime * Random.Range(1.3f, 1.7f), ForceMode.Impulse);
            source.GenerateImpulse(main.transform.forward);
        }

        public void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name != "Player")
            {
                StartCoroutine(Contador());
            }
        }
        #endregion

        IEnumerator Contador()
        {
            yield return new WaitForSeconds(5);
            Destroy(gameObject);
        }

        public void Fire()
        {
            rb.AddForce(transform.forward * fuerza * Time.deltaTime * Random.Range(1.3f, 1.7f), ForceMode.Impulse);
            source = GetComponent<Cinemachine.CinemachineImpulseSource>();
        }

    }
}
